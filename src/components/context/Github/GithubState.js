import React, { useReducer } from "react";
import Axios from "axios";
import GithubContext from "./GithubContext";
import GithubReducer from "./GithubReducer";
import {
  SEARCH_USERS,
  GET_USER,
  CLEAR_USERS,
  GET_REPOS,
  SET_LOADING,
} from "../types";

const GithubState = (props) => {
  const initialState = {
    users: [],
    user: {},
    repos: [],
    loading: false,
  };
  const [state, dispatch] = useReducer(GithubReducer, initialState);

  let gitHubClientId;
  let gitHubClientSecret;
  if (process.env.NODE_ENV !== "production") {
    gitHubClientId = process.env.REACT_APP_GITHUB_CLIENT_ID;
    gitHubClientSecret = process.env.REACT_APP_GITHUB_CLIENT_SECRET;
  } else {
    gitHubClientId = process.env.GITHUB_CLIENT_ID;
    gitHubClientSecret = process.env.GITHUB_CLIENT_SECRET;
  }
  //search users Github
  const searchUsers = async (text) => {
    setLoading();
    const result = await Axios.get(
      `https://api.github.com/search/users?q=${text}&
      client-id=${gitHubClientId}&
      client-secret=${gitHubClientSecret}`,
    );
    dispatch({
      type: SEARCH_USERS,
      payload: result.data.items,
    });
  };

  //Get single Github user
  const getUser = async (username) => {
    setLoading();
    const result = await Axios.get(
      `https://api.github.com/users/${username}?
       client-id=${gitHubClientId}&
      client-secret=${gitHubClientSecret}`,
    );
    dispatch({
      type: GET_USER,
      payload: result.data,
    });
  };
  //Get repository Github's user
  const getUserRepos = async (username) => {
    setLoading();
    const result = await Axios.get(
      `https://api.github.com/users/${username}/repos?per_page=5&sort=created:asc&
       client-id=${gitHubClientId}&
      client-secret=${gitHubClientSecret}`,
    );
    dispatch({
      type: GET_REPOS,
      payload: result.data,
    });
  };

  //clear page
  const clearUsers = () => dispatch({ type: CLEAR_USERS });

  //Define setLoading func
  const setLoading = () => dispatch({ type: SET_LOADING });

  return (
    <GithubContext.Provider
      value={{
        users: state.users,
        user: state.user,
        repos: state.repos,
        loading: state.loading,
        searchUsers,
        getUser,
        clearUsers,
        getUserRepos,
      }}
    >
      {props.children}
    </GithubContext.Provider>
  );
};
export default GithubState;
