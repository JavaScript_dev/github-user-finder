import React, { Fragment } from "react";
import { Link } from "react-router-dom";

import PropTypes from "prop-types";

const Navbar = ({ title, icon, children }) => {
  return (
    <Fragment>
      <div className="navbar bg-primary flex-row-reverse mb-4 align-items-baseline">
        <h5 className="d-flex align-items-center  text-white">
          <span>{title}</span>
          <i className={icon}></i>
        </h5>
        <ul className="wrapperNavItems">
          <li className="navItem">
            <Link className="linkItem" to="/">
              خانه
            </Link>
          </li>
          <li className="navItem">
            <Link className="linkItem" to="/about">
              درباره ما
            </Link>
          </li>
        </ul>
        {children}
      </div>
    </Fragment>
  );
};
Navbar.defaultProps = {
  title: "جستجوگر گیت",
  icon: "fab fa-git-alt brand",
};
Navbar.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};
export default Navbar;
