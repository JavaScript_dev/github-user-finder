import React, { useContext } from "react";
import AlertContext from "../components/context/alert/AlertContext";
export default function Alert() {
  const alertContext = useContext(AlertContext);
  const { alert } = alertContext;
  return (
    alert !== null && (
      <div className="container d-flex justify-content-center">
        <div className={`alert alert-${alert.type} alertBox`}>
          <i className="fas fa-exclamation-circle" />
          {alert.msg}
        </div>
      </div>
    )
  );
}
