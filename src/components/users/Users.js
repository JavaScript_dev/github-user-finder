import React, { useContext } from "react";
import GithubContext from "../context/Github/GithubContext";
import UserItem from "../users/UserItem";
import Spinner from "../spinner/spinner";

const Users = () => {
  const githubContext = useContext(GithubContext);
  const { loading } = githubContext;
  if (loading) {
    return <Spinner />;
  } else {
    return (
      <div className="container-fluid">
        <div className="row">
          {githubContext.users.map((user) => (
            <div className="col-md-3 mb-3" key={user.id}>
              <UserItem user={user} />
            </div>
          ))}
        </div>
      </div>
    );
  }
};

export default Users;
