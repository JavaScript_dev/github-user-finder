import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import GithubContext from "../context/Github/GithubContext";
import Repos from "../repos/Repos";
import Spinner from "../spinner/spinner";
import "../../css/profile.css";

const User = ({ match }) => {
  const githubContext = useContext(GithubContext);
  const { getUser, getUserRepos, user, loading } = githubContext;
  useEffect(() => {
    getUser(match.params.login);
    getUserRepos(match.params.login);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { name, avatar_url, location, bio, login, html_url, hireable } = user;
  if (loading) return <Spinner />;
  return (
    <>
      <div className="profileWrapper">
        <div className="card-container">
          {/* <span className="pro">PRO</span> */}
          <img className="round" src={avatar_url} alt={name} />
          <h3>{name}</h3>
          <h6>{location}</h6>
          <h6>
            {hireable ? (
              <i className="far fa-check-circle success" />
            ) : (
              <i className="far fa-times-circle danger"></i>
            )}
            جویای کار؟{" "}
          </h6>
          {bio && <p>{bio}</p>}
          {login && <p>{login}: نام کاربری </p>}
          <a href={html_url} target="blank" className="GoToGitHub">
            صفحه گیت هاب کاربر
          </a>
          <div className="skills">
            <Link to="/" className="returnButton">
              بازگشت به لیست جستجو
            </Link>
          </div>
        </div>
        <div className="row mt-5 ">
          <Repos />
        </div>
      </div>
    </>
  );
};

export default User;
