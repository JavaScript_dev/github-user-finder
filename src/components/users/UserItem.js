import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const UserItem = ({ user: { login, avatar_url } }) => {
  return (
    <div className="card align-items-center p-3">
      <img src={avatar_url} className="rounded-circle w-25" alt="..." />
      <div className="card-body d-flex flex-column align-items-center">
        <h5 className="card-title text-center">{login}</h5>

        <Link to={`/user/${login}`} className="btn btn-primary">
          مشاهده
        </Link>
      </div>
    </div>
  );
};
UserItem.propTypes = {
  user: PropTypes.object.isRequired,
};
export default UserItem;
