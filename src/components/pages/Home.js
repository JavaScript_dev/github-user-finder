import React, { Fragment } from "react";
import Users from "../users/Users";
import Alert from "../Alert";

const Home = () => (
  <Fragment>
    <Alert />
    <Users />
  </Fragment>
);
export default Home;
