import React, { Fragment } from "react";

const PageNotFound = () => (
  <Fragment>
    <h1>متاسفانه صفحه مورد نظر یافت نشد</h1>
  </Fragment>
);
export default PageNotFound;
