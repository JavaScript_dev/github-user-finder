import React, { Fragment } from "react";
import spinner from "./spinner.gif";
const Spinner = () => {
  return (
    <Fragment>
      <div className="wrapperSpinner">
        <img src={spinner} alt="loading..." />
      </div>
    </Fragment>
  );
};
export default Spinner;
