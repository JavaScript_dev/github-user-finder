import React, { useContext } from "react";
import GithubContext from "../context/Github/GithubContext";
import RepoItem from "./RepoItem";

function Repos() {
  const githubContext = useContext(GithubContext);
  return githubContext.repos.map((repo) => (
    <div key={repo.id} className="col-md-3 mb-3">
      <RepoItem repo={repo} />
    </div>
  ));
}

export default Repos;
