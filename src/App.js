import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavBar from "./components/Navbar.js";
import Search from "./components/Search.js";
import User from "./components/users/User";
import About from "./components/pages/About";
import Home from "./components/pages/Home.js";
import GithubState from "./components/context/Github/GithubState";
import AlertState from "./components/context/alert/AlertState";
import PageNotFound from "./components/pages/PageNotFound.js";

import "./css/App.css";

const App = () => {
  return (
    <Fragment>
      <GithubState>
        <AlertState>
          <Router>
            <NavBar>
              <Search />
            </NavBar>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/about" component={About} />
              <Route exact path="/user/:login" component={User} />
              <Route component={PageNotFound} />
            </Switch>
          </Router>
        </AlertState>
      </GithubState>
    </Fragment>
  );
};

export default App;
